package pl.lakomika.gymfit.entity.invoice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.lakomika.gymfit.entity.CompanyDetails;
import pl.lakomika.gymfit.exceptions.NoDataInPostgresException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Optional;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceCompanyDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String street;

    @Pattern(regexp = "[0-9]{2}-[0-9]{3}", message = "Error postcode")
    private String postcode;

    @NotNull
    private String city;

    @NotNull
    private String accountNumber;

    @NotNull
    private String taxId;

    public static InvoiceCompanyDetails fromCompanyDetails(Optional<CompanyDetails> companyDetails) {
        if (companyDetails.isPresent()) {
            return InvoiceCompanyDetails.builder()
                    .name(companyDetails.get().getName())
                    .accountNumber(companyDetails.get().getAccountNumber())
                    .city(companyDetails.get().getCity())
                    .postcode(companyDetails.get().getPostcode())
                    .street(companyDetails.get().getStreet())
                    .taxId(companyDetails.get().getTaxId())
                    .build();
        } else throw new NoDataInPostgresException("dd");

    }


}
