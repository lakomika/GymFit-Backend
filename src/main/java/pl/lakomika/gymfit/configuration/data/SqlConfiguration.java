package pl.lakomika.gymfit.configuration.data;

import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;


@Configuration
public class SqlConfiguration {

    @Value("${spring.data.nameFile}")
    private String nameFile;

    @Value("${spring.data.isLoadDataFromFileToDb}")
    private boolean isLoadDataFromFileToDb;

    @Bean
    public DataSourceInitializer dataSourceInitializer(@Qualifier("dataSource") final DataSource dataSource) {
        if (isLoadDataFromFileToDb) {
            val resourceDatabase = new ResourceDatabasePopulator();
            resourceDatabase.addScript(new ClassPathResource(nameFile));
            val dataSourceInitializer = new DataSourceInitializer();
            dataSourceInitializer.setDataSource(dataSource);
            dataSourceInitializer.setDatabasePopulator(resourceDatabase);
            return dataSourceInitializer;
        } else {
            return null;
        }
    }
}
