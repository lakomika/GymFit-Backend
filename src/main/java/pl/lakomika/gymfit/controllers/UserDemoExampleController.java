package pl.lakomika.gymfit.controllers;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lakomika.gymfit.services.UserDemoExampleService;

@CrossOrigin
@RestController
@RequestMapping("/api/example/user/")
public class UserDemoExampleController {

    private final UserDemoExampleService userDemoExampleService;

    public UserDemoExampleController(UserDemoExampleService userDemoExampleService) {
        this.userDemoExampleService = userDemoExampleService;
    }

    @GetMapping("/reset-example-data-users")
    public void resetExampleDataUsers() {
        userDemoExampleService.resetExampleDataUsers();
    }
}
