package pl.lakomika.gymfit.dtos.receptionist;

import lombok.Data;
import pl.lakomika.gymfit.entity.Receptionist;

@Data
public class ReceptionistCreateRequest {
    private String email;

    private Receptionist receptionist;
}
