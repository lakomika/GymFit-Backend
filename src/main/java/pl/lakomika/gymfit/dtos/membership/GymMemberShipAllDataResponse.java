package pl.lakomika.gymfit.dtos.membership;

import lombok.Data;
import lombok.val;
import org.modelmapper.ModelMapper;
import pl.lakomika.gymfit.entity.GymMembership;

import java.math.BigDecimal;

@Data
public class GymMemberShipAllDataResponse {
    private String name;

    private BigDecimal priceTotalGross;

    private BigDecimal priceMonthGross;

    private int numberOfMonths;

    private String typeOfMembership;

    private Short taxRatePercent;

    public static GymMemberShipAllDataResponse toResponse(GymMembership gymMembership) {
        val modelMapper = new ModelMapper();
        return modelMapper.map(gymMembership, GymMemberShipAllDataResponse.class);
    }
}
