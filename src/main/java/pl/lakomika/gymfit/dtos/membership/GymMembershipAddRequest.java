package pl.lakomika.gymfit.dtos.membership;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GymMembershipAddRequest {
    private String name;

    private BigDecimal priceMonthGross;

    private Short numberOfMonths;

    private String typeOfMembership;

}
