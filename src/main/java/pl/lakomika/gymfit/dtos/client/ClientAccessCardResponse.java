package pl.lakomika.gymfit.dtos.client;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientAccessCardResponse {
    private Long numberAccessCard;

}
