package pl.lakomika.gymfit.utils;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.lakomika.gymfit.entity.MyUserDetails;

@UtilityClass
public class UserAppData {

    public Long getId() {
        val user = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getId();
    }

    public String getUsername() {
        val user = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

}
