package pl.lakomika.gymfit.exceptions;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class NoDataInPostgresException extends RuntimeException {
    public NoDataInPostgresException(String info) {
        log.warn(info);
    }
}
