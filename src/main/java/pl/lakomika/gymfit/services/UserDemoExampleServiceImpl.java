package pl.lakomika.gymfit.services;

import lombok.val;
import org.springframework.stereotype.Service;
import pl.lakomika.gymfit.entity.UserApp;
import pl.lakomika.gymfit.repository.UserAppRepository;


@Service
public class UserDemoExampleServiceImpl implements UserDemoExampleService {

    private final UserAppRepository userAppRepository;


    public UserDemoExampleServiceImpl(UserAppRepository userAppRepository) {
        this.userAppRepository = userAppRepository;
    }

    @Override
    public void resetExampleDataUsers() {
        val client = UserApp
                .builder()
                .active(true)
                .email("cl.gymfit.inz@gmail.com")
                .password("$2a$10$ZwLVD9/nFIcFkAmWg0I56utPMGLIijqSbuCxdjVvpR9QF7i1E0wWm")
                .id(2L)
                .build();
        userAppRepository.updateUserPassword(client.getPassword(), client.getId());
        userAppRepository.changeUserStatus(client.isActive(), client.getId());
        val receptionist = UserApp
                .builder()
                .active(true)
                .email("re.gymfit.inz@gmail.com")
                .password("$2a$10$vCCzUFbXgd.fKHf3PztLnuixQq.xYF2dCDrd71DjoAbEyL6iiYHIi")
                .id(3L)
                .build();
        userAppRepository.updateUserPassword(receptionist.getPassword(), receptionist.getId());
        userAppRepository.changeUserStatus(receptionist.isActive(), receptionist.getId());
        val admin = UserApp
                .builder()
                .active(true)
                .email("ad.gymfit.inz@gmail.com")
                .password("$2a$10$FDz4ttlUsZKJQzlyQbGKYuA8NbnKw/asRYwB9lqeUUcVo1OimQC3O")
                .id(1L)
                .build();
        userAppRepository.updateUserPassword(admin.getPassword(), admin.getId());
        userAppRepository.changeUserStatus(admin.isActive(), admin.getId());
    }
}
