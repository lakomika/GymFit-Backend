package pl.lakomika.gymfit.services;

import org.springframework.data.domain.Page;
import pl.lakomika.gymfit.dtos.membership.GymMemberShipAllDataResponse;
import pl.lakomika.gymfit.dtos.membership.GymMemberShipsResponse;
import pl.lakomika.gymfit.dtos.membership.GymMembershipAddRequest;
import pl.lakomika.gymfit.entity.GymMembership;

public interface GymMembershipService {
    void save(GymMembershipAddRequest gymMembershipRequest);

    Page<GymMemberShipsResponse> findAllByStatus(int page, int size, boolean status);

    void update(GymMembership gymMembership);

    void changeStatus(Long id, boolean status);

    GymMemberShipAllDataResponse getActiveGymPassById(Long id);
}
