INSERT INTO company_details (id, account_number,
                             street, city,
                             name, postcode, tax_id)
VALUES (1, '11114015601081110181488249',
        'ul. Przykładowa 21/96', 'Warszawa',
        'Zdrofitus sp. z.o.o', '00-950', '1231231234');

INSERT INTO tax(rate_percent, start_validity)
VALUES (8, current_timestamp);

insert into administrator_data (id)
values (1);

INSERT INTO user_app (id, active, email, administrator_id, password, role, username)
VALUES ('1'::bigint, true::boolean, 'ad.gymfit.inz@gmail.com'::character varying, '1'::bigint,
        '$2a$10$FDz4ttlUsZKJQzlyQbGKYuA8NbnKw/asRYwB9lqeUUcVo1OimQC3O'::character varying, --password:  SuperSecretPassword532#
        'ROLE_ADMIN'::character varying, 'admin.example'::character varying)
returning id;

INSERT INTO client_data (id, date_of_birth, city, name, number_card, phone_number, postcode, street, surname)
VALUES ('1'::bigint, '2006-05-02 00:00:00'::timestamp without time zone, 'Warszawa'::character varying,
        'Alicja'::character varying, '1621720879447'::bigint, '789456257'::integer, '02-951'::character varying,
        'Ala ma kota 4/21'::character varying, 'Rudowska'::character varying)
returning id;

INSERT INTO user_app (id, active, email, client_id, password, role, username)
VALUES ('2'::bigint, true::boolean, 'cl.gymfit.inz@gmail.com'::character varying, '1'::bigint,
        '$2a$10$ZwLVD9/nFIcFkAmWg0I56utPMGLIijqSbuCxdjVvpR9QF7i1E0wWm'::character varying, --password: CustomerSecretPassword532#
        'ROLE_CLIENT'::character varying, 'customer.example'::character varying)
returning id;

INSERT INTO receptionist_data (id, city, name, phone_number, postcode, street, surname)
VALUES ('1'::bigint, 'Warszawa'::character varying, 'Patryk'::character varying, '785456984'::integer,
        '21-402'::character varying, 'Przykładowa ulica 54'::character varying,
        'Kowalczyk'::character varying)
returning id;

INSERT INTO user_app (id, active, email, receptionist_id, password, role, username)
VALUES ('3'::bigint, true::boolean, 're.gymfit.inz@gmail.com'::character varying, '1'::bigint,
        '$2a$10$vCCzUFbXgd.fKHf3PztLnuixQq.xYF2dCDrd71DjoAbEyL6iiYHIi'::character varying, --password: ReceptionistSecretPassword532#
        'ROLE_RECEPTIONIST'::character varying, 'receptionist.example'::character varying)
returning id;

SELECT setval('user_app_id_seq', 4, true);

ALTER SEQUENCE user_app_id_seq
    START 4;

SELECT setval('administrator_data_id_seq', 2, true);

SELECT setval('client_data_id_seq', 2, true);

SELECT setval('receptionist_data_id_seq', 2, true);

ALTER SEQUENCE receptionist_data_id_seq
    START 2;

ALTER SEQUENCE administrator_data_id_seq
    START 2;

ALTER SEQUENCE client_data_id_seq
    START 2;