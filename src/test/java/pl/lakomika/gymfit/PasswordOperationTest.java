package pl.lakomika.gymfit;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.lakomika.gymfit.utils.PasswordOperation;

class PasswordOperationTest {

    @Test
    void hashPasswordTest() {
        val bCryptPasswordEncoder = new BCryptPasswordEncoder();
        System.out.println(bCryptPasswordEncoder.encode("ReceptionistSecretPassword532#"));

    }

    @Test
    void generatePasswordTest() {
        val passwordOperation = new PasswordOperation(10);
        System.out.println(passwordOperation.getPassword());
        System.out.println(passwordOperation.getHashPassword());
    }


}
